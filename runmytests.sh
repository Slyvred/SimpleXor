#!/bin/bash
./SimpleXor "main.cpp" "vive le devops"
./SimpleXor "main.cpp"

# Comparer les fichiers `main.cpp` et `main2.cpp`
if cmp -s "main.cpp" "main2.cpp"; then
  echo "Les fichiers main.cpp et main2.cpp sont identiques."
else
  echo "**ERREUR**: Les fichiers main.cpp et main2.cpp sont différents."
  # Optionnel : afficher les différences
  diff "main.cpp" "main2.cpp"
  exit 1  # Indiquer une erreur à l'appelant du script
fi
